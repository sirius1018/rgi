FROM continuumio/miniconda3:4.8.3
RUN apt-get update && apt-get -y upgrade
RUN conda create -n rgi520 -c bioconda -c defaults -c conda-forge rgi=5.2.0
RUN conda init bash
RUN echo "conda activate rgi520" >> ~/.bashrc
ENV PATH /opt/conda/envs/rgi520/bin:$PATH